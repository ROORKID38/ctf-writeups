import hashlib

year = 1800

while year < 2021:
    month = 1
    while month < 13:
        day = 1
        while day < 32:
            strMonth = ''
            strDay = ''
            
            if (month < 10):
                strMonth = '0' + str(month)
            else:
                strMonth = str(month)

            if (day < 10):
                strDay = '0' + str(day)
            else:
                strDay = str(day)
                
            password = 'Aaron' + str(year) + strMonth + strDay
            hashsum = hashlib.md5(password.encode())
            
            if (hashsum.hexdigest() == '7f4986da7d7b52fa81f98278e6ec9dcb'):
                print(password)
                
            day += 1
        month += 1
    year += 1
    
print('DONE')
