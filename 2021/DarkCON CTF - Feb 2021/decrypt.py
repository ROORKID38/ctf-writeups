#!/usr/bin/env python3
from PIL import Image
import base64
import math

# Init image
img = Image.open("inject.png")
obj = img.load()
pixels = []
x, y = 0, 0

# Reading color of the pixels in image
for y in range(150):
	for x in range(150):
		color = obj[x, y]
		if color == (255, 0, 0):
			pixels.append(0)
		elif color == (0, 0, 255):
			pixels.append(1)
		elif color == (0, 128, 0):
			pixels.append(2)
		elif color == (255, 255, 0):
			pixels.append(3)
		elif color == (0, 0, 0):
			continue
		else:
			print('Color not supported (' + str(x) + ', ' + str(y) + ')')

print('[*] Count of pixels           : ' + str(len(pixels)))
print('[*] Count of symbols          : ' + str(len(pixels) // 4))

# Sort pixels
data = []
i = 0
while i < len(pixels):
	data.append(str(pixels[i]) + str(pixels[i + 1]) + str(pixels[i + 2]) + str(pixels[i + 3]))
	i += 4

print('[*] Count of sorted pixels    : ' + str(len(data)))

# Create alphabet
b64symbols = list('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789=+/')
alphabet = []

for sym in b64symbols:
	alphabet.append(str((ord(sym)>>6)&3) + str((ord(sym)>>4)&3) + str((ord(sym)>>2)&3) + str((ord(sym)>>0)&3))

print('[*] Size of the alphabet      : ' + str(len(alphabet)))

# Restore Base64 data
b64data = ''

for code in data:
	symbol = 0
	for num in range(len(alphabet)):
		if code == alphabet[num]:
			b64data += b64symbols[num]
			break

print('[*] Size of the restored data : ' + str(len(b64data)))

if len(b64data) == len(data):
	print('[+] Errors not found')
	print()
	print(b64data)
else:
	print('[-] Error. Exit.')
